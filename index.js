let num1 = parseInt(prompt("Enter any number greater than 50"));
console.log("The number you provided is: " + num1);

for (num1; num1 >= 0; num1--) {
    if (num1 == 50) {
        console.log("The current value is at 50. Terminating the loop.")
        break;
    }

    else if (num1 % 10 == 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }
    else if (num1 % 5 == 0) {
        console.log(num1);
    }
}

let word = "supercalifragilisticexpialidocious";
let wordOutput = '';
console.log(word);

for(x = 0; x < word.length; x++) {
    if ((word[x].toLowerCase() == 'a') || (word[x].toLowerCase() == 'e') || (word[x].toLowerCase() == 'i') || (word[x].toLowerCase() == 'o') || (word[x].toLowerCase() == 'u')) {
        continue;
    }
    else {
        wordOutput = wordOutput + word[x];
    }
}
console.log(wordOutput);